//
// Created by mlujan on 5/23/20.
//

#ifndef _NODOGRAFO_H_
#define _NODOGRAFO_H_
#include "Router.h"

/**
 * V = vertices, es decir plazas (estados, nodos)
 * E = Arcos, se marca con (origen, destino)
 * algo para cambiar asi hago commit
 * */

class NodoGrafo {
 private:
  Router *R;
  int peso;
  int BW;
  int id;
 public:
  NodoGrafo (Router *p, int bw, int id);
  void setRouter (Router *p);
  Router *getRouter ();
  void setPeso (int);
  int getPeso ();
  int getID ();
};

#endif //_NODOGRAFO_H_
